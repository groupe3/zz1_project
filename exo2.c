/*#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

void end_sdl(char ok,char const* msg,SDL_Window* window,SDL_Renderer* renderer) {    
    char msg_formated[255];                                            
    int l; 
    if (!ok) {                                                        
        strncpy(msg_formated, msg, 250);                                         
        l = strlen(msg_formated);                                            
        strcpy(msg_formated + l, " : %s\n");  
        SDL_Log(msg_formated, SDL_GetError());                                   
    }
    if (renderer != NULL) {                                          
     SDL_DestroyRenderer(renderer);                                 
     renderer = NULL;
    }
    if (window != NULL)   {                                          
        SDL_DestroyWindow(window);                                      
        window= NULL;
    }
                                            
    SDL_Quit();                                                                                         
    if (!ok) {                                                  
        exit(EXIT_FAILURE);                                                  
    }                                                          
} 
int setWindowColor(SDL_Renderer *renderer, SDL_Color color) {
  if(SDL_SetRenderDrawColor(renderer, color.r, color.g,color.b, color.a) < 0)
    return -1;
  if(SDL_RenderClear(renderer) < 0)
     return -1;
  return 0;
}

/*void drawdamesdl(SDL_Renderer* renderer ,SDL_Color color,int offset){
    
    setWindowColor(renderer,color);
    SDL_Rect rect[50];
    size_t i = 0;
    for(i = 0; i < 50; i++){
        rect[i].w = 50;
        rect[i].h = 50;
        rect[i].x = 100 * ((i + offset) % 5) + 50 * (((i + offset) / 5) % 2);
        rect[i].y = 50 * ((i + offset) / 5);
    }
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); 
    SDL_RenderFillRects(renderer, rect, 50); 
}  */
/*void drawdamesdl(SDL_Renderer* renderer, SDL_Color* colors, int offset, int screen_width, int screen_height) {
    SDL_Rect rect[100];
    size_t i = 0;
    int square_size = screen_width / 10;  // Adjust square size based on window width
    for (i = 0; i < 100; i++) {
        rect[i].w = square_size;
        rect[i].h = square_size;
        rect[i].x = square_size * ((i + offset) % 10) + square_size * (((i + offset) / 10) % 2);
        rect[i].y = square_size * ((i + offset) / 10);
    }

    for (i = 0; i < 50; i++) {
        SDL_SetRenderDrawColor(renderer, colors[(i + offset) % 5].r, colors[(i + offset) % 5].g, colors[(i + offset) % 5].b, colors[(i + offset) % 5].a);
        SDL_RenderFillRect(renderer, &rect[i]);
    }
}
 

int main(int argc, char** argv) {
    (void)argc;
    (void)argv;
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_DisplayMode screen;
    SDL_Color orange ={255, 127, 40, 255} ;
     SDL_Color colors[5] = {
        {255, 127, 40, 255},  // Orange
        {127, 255, 0, 255},   // Green
        {0, 127, 255, 255},   // Blue
        {255, 0, 127, 255},   // Pink
        {127, 0, 255, 255}    // Purple
    };
    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);
    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n",screen.w, screen.h);
    window = SDL_CreateWindow("Premier dessin",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, screen.w * 0.66,screen.h * 0.66,SDL_WINDOW_OPENGL);
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    /* Création du renderer */
    /*renderer = SDL_CreateRenderer(window, -1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);
    int running = 1;
    SDL_Event event;
    int offset = 0;
    while (running) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = 0;
            }
        }
       /* drawdamesdl(renderer, orange, offset);
        SDL_RenderPresent(renderer);
        offset = (offset + 1) % 50;
        SDL_Delay(100);*/
        //int w, h;
        /*SDL_GetWindowSize(window, &w, &h);
        drawdamesdl(renderer, colors, offset, w, h);
        SDL_RenderPresent(renderer);
        offset = (offset + 1) % 100;
        SDL_Delay(100);
    
    }

    
    end_sdl(1, "Normal ending", window, renderer);
    return EXIT_SUCCESS;
}*/


#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

void end_sdl(char ok, const char* msg, SDL_Window* window, SDL_Renderer* renderer) {    
    char msg_formated[255];                                            
    int l; 
    if (!ok) {                                                        
        strncpy(msg_formated, msg, 250);                                         
        l = strlen(msg_formated);                                            
        strcpy(msg_formated + l, " : %s\n");  
        SDL_Log(msg_formated, SDL_GetError());                                   
    }
    if (renderer != NULL) {                                          
        SDL_DestroyRenderer(renderer);                                 
        renderer = NULL;
    }
    if (window != NULL) {                                          
        SDL_DestroyWindow(window);                                      
        window = NULL;
    }
                                            
    SDL_Quit();                                                                                         
    if (!ok) {                                                  
        exit(EXIT_FAILURE);                                                  
    }                                                          
} 

int setWindowColor(SDL_Renderer *renderer, SDL_Color color) {
    if (SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a) < 0)
        return -1;
    if (SDL_RenderClear(renderer) < 0)
        return -1;
    return 0;
}

void drawdamesdl(SDL_Renderer* renderer, SDL_Color* colors, int offset, int screen_width, int screen_height) {
    SDL_Rect rect[100];
    size_t i = 0;
    int square_size = screen_width / 10;  // Adjust square size based on window width
    for (i = 0; i < 100; i++) {
        rect[i].w = square_size;
        rect[i].h = square_size;
        rect[i].x = square_size * ((i + offset) % 10) + square_size * (((i + offset) / 10) % 2);
        rect[i].y = square_size * ((i + offset) / 10);
    }

    for (i = 0; i < 50; i++) {
        SDL_SetRenderDrawColor(renderer, colors[(i + offset) % 5].r, colors[(i + offset) % 5].g, colors[(i + offset) % 5].b, colors[(i + offset) % 5].a);
        SDL_RenderFillRect(renderer, &rect[i]);
    }
    SDL_RenderPresent(renderer);
}

int main(int argc, char** argv) {
    (void)argc;
    (void)argv;
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_DisplayMode screen;
    SDL_Color colors[5] = {
        {255, 127, 40, 255},  // Orange
        {127, 255, 0, 255},   // Green
        {0, 127, 255, 255},   // Blue
        {255, 0, 127, 255},   // Pink
        {127, 0, 255, 255}    // Purple
    };

    if (SDL_Init(SDL_INIT_VIDEO) != 0) 
        end_sdl(0, "ERROR SDL INIT", window, renderer);
    
    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w, screen.h);
    
    window = SDL_CreateWindow("Premier dessin", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screen.w * 0.66, screen.h * 0.66, SDL_WINDOW_OPENGL);
    if (window == NULL) 
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) 
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);
    
    int running = 1;
    SDL_Event event;
    int offset = 0;

    while (running) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = 0;
            }
        }
        
        int w, h;
        SDL_GetWindowSize(window, &w, &h);
        drawdamesdl(renderer, colors, offset, w, h);
                SDL_RenderPresent(renderer);

        offset = (offset + 1) % 100;
        SDL_Delay(100);
    }

    end_sdl(1, "Normal ending", window, renderer);
    return EXIT_SUCCESS;
}

