#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_CIRCLES 30

typedef struct {
    int x, y;
    int radius;
    int dx, dy;
    SDL_Color color;
} Circle;

void end_sdl(char ok, const char* msg, SDL_Window* window, SDL_Renderer* renderer) {    
    char msg_formated[255];                                            
    int l; 
    if (!ok) {                                                        
        strncpy(msg_formated, msg, 250);                                         
        l = strlen(msg_formated);                                            
        strcpy(msg_formated + l, " : %s\n");  
        SDL_Log(msg_formated, SDL_GetError());                                   
    }
    if (renderer != NULL) {                                          
        SDL_DestroyRenderer(renderer);                                 
        renderer = NULL;
    }
    if (window != NULL) {                                          
        SDL_DestroyWindow(window);                                      
        window = NULL;
    }
                                            
    SDL_Quit();                                                                                         
    if (!ok) {                                                  
        exit(EXIT_FAILURE);                                                  
    }                                                          
} 

int setWindowColor(SDL_Renderer *renderer, SDL_Color color) {
    if (SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a) < 0)
        return -1;
    if (SDL_RenderClear(renderer) < 0)
        return -1;
    return 0;
}

void drawCircle(SDL_Renderer* renderer, int x, int y, int radius, SDL_Color color) {
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    for (int w = 0; w < radius * 2; w++) {
        for (int h = 0; h < radius * 2; h++) {
            int dx = radius - w; // horizontal offset
            int dy = radius - h; // vertical offset
            if ((dx * dx + dy * dy) <= (radius * radius)) {
                SDL_RenderDrawPoint(renderer, x + dx, y + dy);
            }
        }
    }
}

void initCircles(Circle circles[], int num_circles, int screen_width, int screen_height) {
    SDL_Color colors[5] = {
        {255, 127, 40, 255},  // Orange
        {127, 255, 0, 255},   // Green
        {0, 127, 255, 255},   // Blue
        {255, 0, 127, 255},   // Pink
        {127, 0, 255, 255}    // Purple
    };

    srand(time(NULL));
    for (int i = 0; i < num_circles; i++) {
        circles[i].radius = 30 + rand() % 20;
        circles[i].x = circles[i].radius + rand() % (screen_width - 2 * circles[i].radius);
        circles[i].y = circles[i].radius + rand() % (screen_height - 2 * circles[i].radius);
        circles[i].dx = 2 + rand() % 5;
        circles[i].dy = 2 + rand() % 5;
        circles[i].color = colors[rand() % 5];
    }
}

void moveCircles(Circle circles[], int num_circles, int screen_width, int screen_height) {
    for (int i = 0; i < num_circles; i++) {
        circles[i].x += circles[i].dx;
        circles[i].y += circles[i].dy;

        if (circles[i].x - circles[i].radius < 0 || circles[i].x + circles[i].radius > screen_width) {
            circles[i].dx = -circles[i].dx;
        }
        if (circles[i].y - circles[i].radius < 0 || circles[i].y + circles[i].radius > screen_height) {
            circles[i].dy = -circles[i].dy;
        }
    }
}

void drawCircles(SDL_Renderer* renderer, Circle circles[], int num_circles) {
    for (int i = 0; i < num_circles; i++) {
        drawCircle(renderer, circles[i].x, circles[i].y, circles[i].radius, circles[i].color);
    }
}

int main(int argc, char** argv) {
    (void)argc;
    (void)argv;
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_DisplayMode screen;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) 
        end_sdl(0, "ERROR SDL INIT", window, renderer);
    
    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w, screen.h);
    
    window = SDL_CreateWindow("Animation géométrique", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screen.w * 0.66, screen.h * 0.66, SDL_WINDOW_OPENGL);
    if (window == NULL) 
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) 
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    int running = 1;
    SDL_Event event;
    Circle circles[NUM_CIRCLES];

    SDL_GetWindowSize(window, &screen.w, &screen.h);
    initCircles(circles, NUM_CIRCLES, screen.w, screen.h);
    
    while (running) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = 0;
            }
        }

        setWindowColor(renderer, (SDL_Color){255, 255, 255, 255}); // Clear screen with white
        moveCircles(circles, NUM_CIRCLES, screen.w, screen.h);
        drawCircles(renderer, circles, NUM_CIRCLES);
        SDL_RenderPresent(renderer);
        
        SDL_Delay(16); // Approximately 60 frames per second
    }

    end_sdl(1, "Normal ending", window, renderer);
    return EXIT_SUCCESS;
}
