#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string.h>

void end_sdl(char ok, const char* msg, SDL_Window* window, SDL_Renderer* renderer) {
    char msg_formated[255];
    int l;
    if (!ok) {
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");
        SDL_Log(msg_formated, SDL_GetError());
    }
    if (renderer != NULL) {
        SDL_DestroyRenderer(renderer);
        renderer = NULL;
    }
    if (window != NULL) {
        SDL_DestroyWindow(window);
        window = NULL;
    }
    SDL_Quit();
    if (!ok) {
        exit(EXIT_FAILURE);
    }
}

SDL_Texture* load_texture_from_image(const char* file_image_name, SDL_Window* window, SDL_Renderer* renderer) {
    SDL_Surface* my_image = NULL;
    SDL_Texture* my_texture = NULL;

    my_image = IMG_Load(file_image_name);
    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

void render_parallax_layer(SDL_Renderer* renderer, SDL_Texture* texture, int texture_width, int screen_width, int y_pos, float scroll_speed, int* x_offset) {
    SDL_Rect src_rect, dst_rect;
    src_rect.y = 0;
    src_rect.w = texture_width;
    src_rect.h = screen_width;
    
    dst_rect.y = y_pos;
    dst_rect.w = texture_width;
    dst_rect.h = screen_width;
    
    *x_offset -= scroll_speed;
    if (*x_offset <= -texture_width) {
        *x_offset = 0;
    }
    
    src_rect.x = *x_offset;
    dst_rect.x = 0;
    
    SDL_RenderCopy(renderer, texture, &src_rect, &dst_rect);
    
    src_rect.x = 0;
    dst_rect.x = texture_width + *x_offset;
    
    SDL_RenderCopy(renderer, texture, &src_rect, &dst_rect);
}

int main(int argc, char** argv) {
    (void)argc;
    (void)argv;
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_DisplayMode screen;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        end_sdl(0, "ERROR SDL INIT", window, renderer);
    }

    if (IMG_Init(IMG_INIT_PNG) == 0) {
        end_sdl(0, "ERROR SDL IMG INIT", window, renderer);
    }

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw: %d\n\th: %d\n", screen.w, screen.h);
    window = SDL_CreateWindow("Parallax Scrolling", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screen.w, screen.h, SDL_WINDOW_FULLSCREEN);
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    SDL_Texture* background = load_texture_from_image("background.png", window, renderer);
    SDL_Texture* midground = load_texture_from_image("midground.png", window, renderer);
    SDL_Texture* foreground = load_texture_from_image("foreground.png", window, renderer);

    int bg_width, bg_height;
    int mg_width, mg_height;
    int fg_width, fg_height;
    SDL_QueryTexture(background, NULL, NULL, &bg_width, &bg_height);
    SDL_QueryTexture(midground, NULL, NULL, &mg_width, &mg_height);
    SDL_QueryTexture(foreground, NULL, NULL, &fg_width, &fg_height);

    int bg_x_offset = 0;
    int mg_x_offset = 0;
    int fg_x_offset = 0;

    int running = 1;
    SDL_Event event;

    while (running) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = 0;
            }
        }

        SDL_RenderClear(renderer);

        render_parallax_layer(renderer, background, bg_width, screen.w, 0, 1.0, &bg_x_offset);
        render_parallax_layer(renderer, midground, mg_width, screen.w, 0, 2.0, &mg_x_offset);
        render_parallax_layer(renderer, foreground, fg_width, screen.w, 0, 3.0, &fg_x_offset);

        SDL_RenderPresent(renderer);
        SDL_Delay(16); // Roughly 60 frames per second
    }

    SDL_DestroyTexture(background);
    SDL_DestroyTexture(midground);
    SDL_DestroyTexture(foreground);
    end_sdl(1, "Normal ending", window, renderer);
    return 0;
}
