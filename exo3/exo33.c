#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void end_sdl(char ok, const char* msg, SDL_Window* window, SDL_Renderer* renderer) {
    char msg_formated[255];
    int l;
    if (!ok) {
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");
        SDL_Log(msg_formated, SDL_GetError());
    }
    if (renderer != NULL) {
        SDL_DestroyRenderer(renderer);
        renderer = NULL;
    }
    if (window != NULL) {
        SDL_DestroyWindow(window);
        window = NULL;
    }
    SDL_Quit();
    if (!ok) {
        exit(EXIT_FAILURE);
    }
}

SDL_Texture* load_texture_from_image(const char *file_image_name, SDL_Window *window, SDL_Renderer *renderer) {
    SDL_Texture* my_texture = NULL;
    SDL_Surface *image_surface = NULL;
    image_surface = IMG_Load(file_image_name);
    if (!image_surface) {
        fprintf(stderr, "Error loading image: %s\n", IMG_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        return NULL;
    }

    my_texture = SDL_CreateTextureFromSurface(renderer, image_surface);
    SDL_FreeSurface(image_surface);

    if (!my_texture) {
        fprintf(stderr, "Error creating texture: %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        return NULL;
    }

    return my_texture;
}

void play_with_texture_1(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer) {
    SDL_Rect source = {0}, window_dimensions = {0}, destination = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    destination = window_dimensions;

    SDL_RenderCopy(renderer, my_texture, &source, &destination);
    SDL_RenderPresent(renderer);
}

void play_with_texture_4(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer) {
    SDL_Rect source = {0}, window_dimensions = {0}, destination = {0}, state = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    int nb_images = 8;
    float zoom = 5;
    int offset_x = source.w / nb_images;
    int offset_y = source.h / 2;

    state.x = 0;
    state.y = 3 * offset_y;
    state.w = offset_x;
    state.h = offset_y;

    destination.w = offset_x * zoom;
    destination.h = offset_y * zoom;
    destination.y = (window_dimensions.h - destination.h) / 2;

    int speed = 9;
    for (int x = 0; x < window_dimensions.w - destination.w; x += speed) {
        destination.x = x;
        state.x += offset_x;
        state.x %= source.w;

        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, my_texture, &state, &destination);
        SDL_RenderPresent(renderer);
        SDL_Delay(80);
    }
    SDL_RenderClear(renderer);
}

void play_with_texture_5(SDL_Texture* bg_texture, SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer, int frame) {
    SDL_Rect source = {0}, window_dimensions = {0}, destination = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    int nb_images = 77; // Nombre total d'images dans la feuille de sprite
    float zoom = 10;
    int offset_x = source.w / 7; // Nombre de colonnes dans la feuille de sprite
    int offset_y = source.h / 11; // Nombre de lignes dans la feuille de sprite
    SDL_Rect state[77]; // Tableau pour stocker les positions de chaque image dans la feuille de sprite

    // Remplissage du tableau state avec les positions de chaque image dans la feuille de sprite
    int i = 0;
    for (int y = 0; y < source.h; y += offset_y) {
        for (int x = 0; x < source.w; x += offset_x) {
            state[i].x = x;
            state[i].y = y;
            state[i].w = offset_x;
            state[i].h = offset_y;
            ++i;
        }
    }

    destination.w = offset_x * zoom; // Largeur du sprite à l'écran
    destination.h = offset_y * zoom; // Hauteur du sprite à l'écran
    destination.x = 0; // Position en x pour l'affichage du sprite
    destination.y = window_dimensions.h * 0.7; // Position en y pour l'affichage du sprite

    int speed = 5; // Vitesse de déplacement horizontale de l'image

    SDL_Event event;
    SDL_bool running = SDL_TRUE;

    while (running) {
        // Gestion des événements
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = SDL_FALSE;
            }
        }
        i = 0;
        for (int cpt = 0; cpt < nb_images; ++cpt) {
            play_with_texture_1(bg_texture, window, renderer);
            // Dessin de l'arrière-plan avant d'afficher le sprite animé
            SDL_RenderCopy(renderer, bg_texture, NULL, NULL);

            // Affichage du sprite animé à la position actuelle
            SDL_RenderCopy(renderer, my_texture, &state[frame % nb_images], &destination);
            i = (i + 1) % nb_images; 
            frame = (frame + 1) % nb_images;
            // Affichage du rendu
            SDL_RenderPresent(renderer);
            // Déplacement horizontal de l'image
            destination.x += speed;

            // Si l'image sort de l'écran à droite, revenir à la position de départ à gauche
            if (destination.x > window_dimensions.w) {
                destination.x = -destination.w;
            }

            SDL_Delay(100); // Pause en ms pour contrôler la vitesse de défilement
        }
    }
}

int main(int argc, char** argv) {
    (void)argc;
    (void)argv;
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_DisplayMode screen;
    int frame = 0;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        end_sdl(0, "ERROR SDL INIT", window, renderer);
    }

    if (IMG_Init(IMG_INIT_PNG) == 0) {
        end_sdl(0, "ERROR SDL IMG INIT", window, renderer);
    }

    SDL_GetCurrentDisplayMode(0, &screen);
    window = SDL_CreateWindow("Sprite Animation", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screen.w, screen.h, SDL_WINDOW_EXO;
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    SDL_Texture* bg_texture = load_texture_from_image("bg.png", window, renderer);
    if (!bg_texture) {
        end_sdl(0, "ERROR LOADING bg", window, renderer);
    }

    SDL_Texture* sprite_texture = load_texture_from_image("sprite.png", window, renderer);
    if (!sprite_texture) {
        end_sdl(0, "ERROR LOADING SPRITE", window, renderer);
    }

    // Charger la texture du bouton de fermeture
    SDL_Texture* close_button_texture = load_texture_from_image("close_button.png", window, renderer);
    if (!close_button_texture) {
        end_sdl(0, "ERROR LOADING CLOSE BUTTON", window, renderer);
    }

    SDL_Rect close_button_rect;
    close_button_rect.w = 50; // Largeur du bouton
    close_button_rect.h = 50; // Hauteur du bouton
    close_button_rect.x = screen.w - close_button_rect.w - 10; // Position en x (10 pixels de la bordure droite)
    close_button_rect.y = 10; // Position en y (10 pixels de la bordure supérieure)

    SDL_bool program_on = SDL_TRUE; // Booléen pour dire que le programme doit continuer
    SDL_Event event;

    while (program_on) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    program_on = SDL_FALSE; // Arrêter le programme si l'utilisateur ferme la fenêtre
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    if (event.button.button == SDL_BUTTON_LEFT) {
                        int x = event.button.x;
                        int y = event.button.y;
                        if (x >= close_button_rect.x && x <= close_button_rect.x + close_button_rect.w &&
                            y >= close_button_rect.y && y <= close_button_rect.y + close_button_rect.h) {
                            program_on = SDL_FALSE; // Arrêter le programme si le bouton de fermeture est cliqué
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        // Dessiner l'arrière-plan
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, bg_texture, NULL, NULL);

        // Appel de la fonction d'animation avec le numéro de séquence actuel
        play_with_texture_5(bg_texture, sprite_texture, window, renderer, frame);

        // Dessiner le bouton de fermeture
        SDL_RenderCopy(renderer, close_button_texture, NULL, &close_button_rect);

        SDL_RenderPresent(renderer);
        SDL_Delay(100);

        // Incrémentation du numéro de séquence pour la prochaine image
        frame = (frame + 1) % 77; // 77 est le nombre total d'images dans la feuille de sprite
    }

    SDL_DestroyTexture(sprite_texture);
    SDL_DestroyTexture(bg_texture);
    SDL_DestroyTexture(close_button_texture); // Détruire la texture du bouton de fermeture
    IMG_Quit();
    end_sdl(1, "Normal ending", window, renderer);
    return 0;
}
